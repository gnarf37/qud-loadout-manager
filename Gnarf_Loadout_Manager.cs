using System;
using System.Text;
using System.Collections.Generic;
using XRL.World;
using XRL.World.Anatomy;

namespace XRL.World.Parts
{
    [Serializable]
    [XRL.Wish.HasWishCommand]
    public class Gnarf_Loadout_Manager : IPart
    {
        public class Gnarf_Loadout
        {
            public string Name = "New Loadout";
            public string CommandID = null;
            public Guid AbilityID = Guid.Empty;

            public List<Gnarf_Loadout_Part> Parts = new List<Gnarf_Loadout_Part>(0);
        }

        [Serializable]
        public class Gnarf_Loadout_Part
        {
            public string PartName;
            public string EquipID = "";
            public string EquipBlueprint = "";
            [NonSerialized] public GameObject GO;
        }

        // Custom serialization
        [NonSerialized]
        public List<Gnarf_Loadout> Loadouts = new List<Gnarf_Loadout>(0);

        public int LoadoutCounter = 0;

        public string NextCommandId()
        {
            return "Gnarf_Cmd_Loadout_" + (LoadoutCounter++);
        }

        public Gnarf_Loadout_Manager()
        {
        }

        [XRL.Wish.WishCommand]
        public static bool Open_Loadout()
        {
            IPart.ThePlayer.RequirePart<Gnarf_Loadout_Manager>().ManageLoadouts();
            return true;
        }

        public override void SaveData(SerializationWriter Writer)
        {
            int loCount = Loadouts.Count; 
            Writer.Write(loCount);
            for (int x =0; x<loCount; x++) {
                Gnarf_Loadout lo = Loadouts[x];
                Writer.Write(lo.Name);
                Writer.Write(lo.CommandID);
                Writer.Write(lo.AbilityID);
                Writer.Write(lo.Parts.Count);
                foreach(var lp in lo.Parts) {
                    Writer.Write(lp.PartName);
                    Writer.Write(lp.EquipBlueprint);
                    Writer.Write(lp.EquipID);
                }
            }
            base.SaveData(Writer);
        }

        public override void LoadData(SerializationReader Reader)
        {
            int loCount = Reader.ReadInt32(); 
            Loadouts = new List<Gnarf_Loadout>(loCount);
            for(int x = 0; x<loCount; x++)
            {
                Gnarf_Loadout lo = new Gnarf_Loadout();
                lo.Name = Reader.ReadString();
                lo.CommandID = Reader.ReadString();
                lo.AbilityID = Reader.ReadGuid();
                int lpCount = Reader.ReadInt32();
                lo.Parts = new List<Gnarf_Loadout_Part>(lpCount);
                for(int z=0; z<lpCount; z++) {
                    Gnarf_Loadout_Part lp = new Gnarf_Loadout_Part();
                    lp.PartName = Reader.ReadString();
                    lp.EquipBlueprint = Reader.ReadString();
                    lp.EquipID = Reader.ReadString();
                    lo.Parts.Add(lp);
                }
                Loadouts.Add(lo);
            }

            base.LoadData(Reader);
        } 

        public ActivatedAbilities pAA()
        {
            return ParentObject.GetPart("ActivatedAbilities") as ActivatedAbilities;
        }

        public override bool WantEvent(int ID, int Cascade)
        {
            return 
                ID == CommandEvent.ID ||
                ID == ObjectCreatedEvent.ID ||
                ID == BeforeTakeActionEvent.ID ||
                base.WantEvent(ID, Cascade);
        }
        public void Enhance()
        {
            ActivatedAbilities Abilities = pAA();
            Abilities.AddAbility(
                "Manage Equipment Loadouts",
                "Gnarf_Cmd_Manage_Loadout",
                "Loadouts",
                Description: "Create or remove equipment loadouts.",
                Icon: "-",
                Silent: true
            );
        }

        public string ChoiceForPart(BodyPart part) {
            if (part.Equipped != null) {
                return part.GetCardinalDescription() + ":\n " + part.Equipped.DisplayName;
            } else {
                return part.GetCardinalDescription() + ":\n Empty";
            }
        }

        public void EditLoadout(Gnarf_Loadout Loadout)
        {
            Body pBody = ParentObject.GetPart("Body") as Body;
            List<BodyPart> parts = pBody.GetParts();

            foreach (Gnarf_Loadout_Part lp in Loadout.Parts)
            {
                GameObject go = pBody.GetPartByName(lp.PartName).Equipped;
                if (go != null) {
                    lp.EquipID = go.ID;
                    lp.EquipBlueprint = go.Blueprint;
                } else {
                    lp.EquipID = "";
                    lp.EquipBlueprint = "";
                }
            }

            bool bDone = false;

            while (!bDone) {
                char c = 'a';
                List<string> Options = new List<string>(parts.Count + 1);
                List<char> keymap = new List<char>(parts.Count + 1);
                List<string> cmds = new List<string>(parts.Count + 1);
                if (Loadout.Parts.Count > 0)
                {
                    Options.Add("Clear Equipment");
                    keymap.Add('X');
                    cmds.Add("clear");
                    Options.Add("Save Loadout");
                    keymap.Add('S');
                    cmds.Add("save");
                }


                string PartsSaved = "";

                foreach(BodyPart part in parts) {
                    if (Loadout.Parts.Exists((lp) => lp.PartName == part.Name)) {
                        PartsSaved += ChoiceForPart(part) + "\n";
                    } else {
                        Options.Add(ChoiceForPart(part));
                        keymap.Add(c++);
                        cmds.Add("" + part.ID);
                    }
                }

                int choice = XRL.UI.Popup.ShowOptionList(
                    "Loadout Creator: Pick equipment to save in this loadout.",
                    Options.ToArray(),
                    keymap.ToArray(),
                    1,
                    "Currently Saved " + Loadout.Parts.Count + " parts\n" + 
                    (Loadout.Parts.Count > 5 ? "(...everything not listed to pick...)" : PartsSaved),
                    74, true, true, 0
                );

                if (choice == -1) {
                    return;
                }
                
                string cmd = cmds[choice];
                if (cmd == "clear") {
                    Loadout.Parts.Clear();
                }
                else if (cmd == "save") {
                    if (Loadout.CommandID == null)
                    {
                        Loadout.CommandID = NextCommandId();

                    }
                    Loadout.Name = XRL.UI.Popup.AskString("Enter a name for the Loadout:", Loadout.Name, MaxLength: 60);
                    if (Loadout.AbilityID == Guid.Empty)
                    {
                        Loadout.AbilityID = pAA().AddAbility(
                            Loadout.Name,
                            Loadout.CommandID,
                            "Loadouts",
                            Description: PartsSaved,
                            Icon: "-",
                            Silent: true
                        );
                        Loadouts.Add(Loadout);
                        ParentObject.RegisterPartEvent(this, Loadout.CommandID);
                    }
                    else
                    {
                        ActivatedAbilityEntry aa = pAA().GetAbilityByCommand(Loadout.CommandID);
                        aa.DisplayName = Loadout.Name;
                        aa.Description = PartsSaved;                        
                    }
                    bDone = true;
                } else {
                    BodyPart SaveMe = parts.Find((part) => ("" + part.ID) == cmd);
                    Gnarf_Loadout_Part lp = new Gnarf_Loadout_Part();
                    lp.PartName = SaveMe.Name;
                    if (SaveMe.Equipped != null)
                    {
                        if (!SaveMe.Equipped.HasPart("Gnarf_Loadouts_Used")) {
                            SaveMe.Equipped.AddPart(new Gnarf_Loadouts_Used());
                        }
                        lp.EquipID = SaveMe.Equipped.ID;
                        lp.EquipBlueprint = SaveMe.Equipped.Blueprint;
                        // check for multi handed weapons and add all slots they exist on
                        foreach (BodyPart OtherPart in parts)
                        {
                            if (OtherPart.Equipped == SaveMe.Equipped && OtherPart != SaveMe) {
                                Gnarf_Loadout_Part h2 = new Gnarf_Loadout_Part();
                                h2.PartName = OtherPart.Name;
                                h2.EquipID = OtherPart.Equipped.ID;
                                h2.EquipBlueprint = OtherPart.Equipped.Blueprint;
                                Loadout.Parts.Add(h2);
                            }
                        }
                    }
                    Loadout.Parts.Add(lp);
                }
            }

        }

        public void EquipLoadout(Gnarf_Loadout Loadout) {
            Body pBody = ParentObject.GetPart("Body") as Body;

            if (pBody == null)
            {
                return;
            }


            Dictionary<GameObject, List<BodyPart>> itemsToEquip = new Dictionary<GameObject, List<BodyPart>>();
            // Pass one - unequip anything we can in the slots - abort if we cant remove something.

            foreach(Gnarf_Loadout_Part pDesc in Loadout.Parts)
            {
                BodyPart pBP = pBody.GetPartByName(pDesc.PartName);
                if (pBP == null)
                {
                    AddPlayerMessage("&R[Loadout] Missing Body Part: &y" + pDesc.PartName);
                    continue;
                }

                // Goal is Unequipped
                if (pDesc.EquipID == "")
                {
                    if (pBP.Equipped != null)
                    {
                        Qud.API.EquipmentAPI.UnequipObject(pBP.Equipped);
                    }
                    continue;
                }

                if (pBP.Equipped != null && pBP.Equipped.id == pDesc.EquipID)
                {
                    continue;
                }

                GameObject GO = null;

                ParentObject.ForeachInventoryAndEquipment((o) => 
                {
                    if (GO == null || GO.ID != pDesc.EquipID)
                    {
                        if (o.ID == pDesc.EquipID || o.Blueprint == pDesc.EquipBlueprint)
                        {
                            GO = o;
                        }
                    }
                });
                if (GO == null)
                {
                    AddPlayerMessage("&R[Loadout] Missing Equipment for &W" +pBP.GetCardinalDescription());
                    continue;
                }
                if (pBP.Equipped != null)
                {
                    Qud.API.EquipmentAPI.UnequipObject(pBP.Equipped);
                }
                if (itemsToEquip.ContainsKey(GO))
                {
                    itemsToEquip[GO].Add(pBP);
                }
                else
                {
                    itemsToEquip[GO] = new List<BodyPart>(1);
                    itemsToEquip[GO].Add(pBP);
                }
            }

            foreach (KeyValuePair<GameObject, List<BodyPart>> entry in itemsToEquip)
            {
                foreach (BodyPart part in entry.Value)
                {
                    if (part.Equipped != entry.Key)
                    {
                        Qud.API.EquipmentAPI.EquipObject(ParentObject, entry.Key, part);
                    }
                }
            }
        }

        public void ManageLoadouts()
        {
            char c = 'a';
            char c2 = 'A';
            List<string> Options = new List<string>(Loadouts.Count * 2 + 1);
            List<char> keymap = new List<char>(Loadouts.Count * 2 + 1);
            Options.Add("Create new Loadout");
            keymap.Add('.');

            foreach (Gnarf_Loadout Loadout in Loadouts)
            {
                Options.Add("&GUPDATE&y: " + Loadout.Name);
                keymap.Add(c++);
                Options.Add("&RDELETE&y: " + Loadout.Name);
                keymap.Add(c2++);
            }

            int choice = XRL.UI.Popup.ShowOptionList(
                "Welcome to the loadout manager",
                Options.ToArray(),
                keymap.ToArray(),
                0,
                "This will create custom abilities to equip gear you are currently wearing!  " +
                "If you Create a new Loadout, it will let you select the specific " +
                "equipment you'd like to save in the loadout.",
                60, false, true, 0
            );

            if (choice == -1) {
                return;
            }

            if (choice == 0) {
                EditLoadout(new Gnarf_Loadout());
                return;
            }

            int chosen = (choice-1) / 2;
            bool bDelete = choice % 2 == 0;
            Gnarf_Loadout L = Loadouts[chosen];
            if (bDelete) {
                pAA().RemoveAbility(L.AbilityID);
                Loadouts.Remove(L);
                ParentObject.UnregisterPartEvent(this, L.CommandID);
            } else {
                EditLoadout(L);
            }
        }
        public override bool HandleEvent(ObjectCreatedEvent E)
        {
            if (pAA() == null) {
                ParentObject.RemovePart(this);
                return true;
            }
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(BeforeTakeActionEvent E)
        {
            if (ParentObject.IsPlayer())
            {
                ActivatedAbilities Abilities = pAA();
                if (Abilities.GetAbilityByCommand("Gnarf_Cmd_Manage_Loadout") == null)
                {
                    Enhance();
                }
                return true;            
            }
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(CommandEvent E)
        {
            if (E.Command == "Gnarf_Cmd_Manage_Loadout")
            {
                ManageLoadouts();
                return true;
            }
            if (E.Command.StartsWith("Gnarf_Cmd_Loadout_"))
            {
                Gnarf_Loadout Loadout = Loadouts.Find((lo) => {
                    return lo.CommandID == E.Command;
                });
                if (Loadout == null) {
                    AddPlayerMessage("&R[Loadout] - Can't find loadout configuration - potentially broken save - Sorry!");
                    return true;
                }

                EquipLoadout(Loadout);
            }
            return base.HandleEvent(E);
        }
    }

}